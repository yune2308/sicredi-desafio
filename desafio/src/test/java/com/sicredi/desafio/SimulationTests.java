package com.sicredi.desafio;

import com.sicredi.desafio.core.Constants;
import com.sicredi.desafio.model.ResponseModel;
import com.sicredi.desafio.model.SimulationModel;
import com.sicredi.desafio.service.SimulationService;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@DisplayName("Testes de simulação")
public class SimulationTests {

    @Autowired
    private transient SimulationService simulationService;

    @Test
    @Order(1)
    @DisplayName("Simulação criada com sucesso")
    public void createSimulationSuccessful() {
        ResponseModel response = simulationService.create(SimulationModel.defaultSimulation());
        Assert.isTrue(response.isStatusCreated(), "Ok");
    }

    @Test
    @Order(2)
    @DisplayName("Erro ao criar simulação com dados incorretos")
    public void createSimulationWhitError() {
        SimulationModel simulation = SimulationModel.defaultSimulation();
        simulation.setValor(Constants.VALOR_50000);
        ResponseModel response = simulationService.create(simulation);
        Assert.isTrue(response.isStatusBadRequest(), "Ok");
    }

    @Test
    @Order(3)
    @DisplayName("Erro ao criar simulação com CPF existente")
    public void createSimulationWhitDuplicatedCpf() {
        SimulationModel simulation = SimulationModel.defaultSimulation();
        simulation.setCpf(Constants.CPF_VALID);

        ResponseModel responseOk = simulationService.create(simulation);
        Assert.isTrue(responseOk.isStatusCreated(), "Ok");

        ResponseModel responseError = simulationService.create(simulation);
        Assert.isTrue(responseError.isStatusBadRequest(), "Ok");
    }

    @Test
    @Order(4)
    @DisplayName("Editar simulação com CPF existente")
    void editSimulation() {
        SimulationModel simulation = SimulationModel.defaultSimulation();
        simulation.setEmail(Constants.EMAIL_EXAMPLE);
        ResponseModel response = simulationService.edit(Constants.CPF, simulation);
        Assert.isTrue(response.isStatusOK(), "Ok");
    }

    @Test
    @Order(5)
    @DisplayName("Editar simulação com CPF inexistente")
    void editSimulationCPFInexistente() {
        SimulationModel simulation = SimulationModel.defaultSimulation();
        simulation.setParcelas(5);
        ResponseModel response = simulationService.edit(Constants.CPF_NOT_INSERTED, simulation);
        Assert.isTrue(response.isStatusNotFound(), "Ok");
    }

    @Test
    @Order(6)
    @DisplayName("Obter todas as simulações")
    void getAllSimulations() {
        SimulationModel[] allSimulations = simulationService.getAll();
        Assert.notEmpty(allSimulations, "Ok");
    }

    @Test
    @Order(7)
    @DisplayName("Obter simulação por CPF")
    void getSimulationByCpf() {
        ResponseModel response = simulationService.getByCpf(Constants.CPF);
        Assert.isTrue(response.isStatusOK(), "Ok");
    }

    @Test
    @Order(8)
    @DisplayName("Obter simulação CPF não existe")
    void getSimulationByCpfNotExist() {
        ResponseModel response = simulationService.getByCpf(Constants.CPF_NOT_INSERTED);
        Assert.isTrue(response.isStatusNotFound(), "Ok");
    }

    @Test
    @Order(9)
    @DisplayName("Remover simulação por id válido")
    void deleteSimulation() {
        ResponseModel response = simulationService.delete(Constants.ID);
        Assert.isTrue(response.isStatusOK(), "Ok");
    }

    @Test
    @Order(10)
    @DisplayName("Remover simulação por id null")
    void deleteSimulationIdNull() {
        ResponseModel response = simulationService.delete(null);
        Assert.isTrue(response.isStatusBadRequest(), "Ok");
    }
    @Test
    @Order(11)
    @DisplayName("Verificar se não existir simulações cadastradas")
    void getNoneSimulations() {
        simulationService.deleteAll();
        SimulationModel[] allSimulations = simulationService.getAll();
        Assert.isTrue(allSimulations.length == 0, "Ok");
    }
}

