package com.sicredi.desafio;

import com.sicredi.desafio.core.Constants;
import com.sicredi.desafio.model.ResponseModel;
import com.sicredi.desafio.service.RestrictionService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

@SpringBootTest
@DisplayName("Testes de restrição")
public class RestrictionTests {

	@Autowired private transient RestrictionService restrictionService;

	@Test
	@DisplayName("Verificação de CPF com restrição")
	void verifyCpfWithRestriction() {
		ResponseModel response = restrictionService.consultRestrictionCPF(Constants.CPF_WITH_RESTRICTION);
		Assert.isTrue(response.isStatusOK(), "Ok");
	}

	@Test
	@DisplayName("Verificação de CPF sem restrição")
	void verifyCpfWithoutRestriction() {
		ResponseModel response = restrictionService.consultRestrictionCPF(Constants.CPF);
		Assert.isTrue(response.isStatusNoContent(), "Ok");
	}
}
