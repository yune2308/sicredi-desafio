package com.sicredi.desafio.model;

import com.sicredi.desafio.core.Constants;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

public class SimulationModel {

    @Getter @Setter private Long id;
    @Getter @Setter private String nome;
    @Getter @Setter private String cpf;
    @Getter @Setter private String email;
    @Getter @Setter private BigDecimal valor;
    @Getter @Setter private Integer parcelas;
    @Getter @Setter private Boolean seguro;

    public static SimulationModel defaultSimulation(){
        SimulationModel request = new SimulationModel();
        request.setNome(Constants.NOME);
        request.setCpf(Constants.CPF);
        request.setEmail(Constants.EMAIL);
        request.setValor(Constants.VALOR_5000);
        request.setParcelas(Constants.PARCELAS);
        request.setSeguro(Constants.SEGURO);
        return request;
    }
}
