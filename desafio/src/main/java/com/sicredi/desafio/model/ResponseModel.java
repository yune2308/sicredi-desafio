package com.sicredi.desafio.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ResponseModel {

    @Getter @Setter private HttpStatus status;

    public static ResponseModel buildResponse(ResponseEntity<?> responseEntity) {
        ResponseModel model = new ResponseModel();
        if (responseEntity == null) {
            return model;
        }
        model.setStatus(responseEntity.getStatusCode());
        return model;
    }

    public static ResponseModel buildResponse(HttpStatus status) {
        ResponseModel model = new ResponseModel();
        model.setStatus(status);
        return model;
    }

    // 200
    public boolean isStatusOK() {
        return HttpStatus.OK.equals(this.status);
    }

    // 204
    public boolean isStatusNoContent() {
        return HttpStatus.NO_CONTENT.equals(this.status);
    }

    // 201
    public boolean isStatusCreated() {
        return HttpStatus.CREATED.equals(this.status);
    }

    // 400
    public boolean isStatusBadRequest() {
        return HttpStatus.BAD_REQUEST.equals(this.status);
    }

    // 409
    public boolean isStatusConflict() {
        return HttpStatus.CONFLICT.equals(this.status);
    }

    // 404
    public boolean isStatusNotFound() {
        return HttpStatus.NOT_FOUND.equals(this.status);
    }
}
