package com.sicredi.desafio.service;

import com.sicredi.desafio.core.BuildPropertiesResolved;
import com.sicredi.desafio.model.ResponseModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;

@Service
public class RestrictionService {

    public static final String PATH_API_V1_RESTRICTION = "/api/v1/restricoes/";
    private final RestTemplate template;
    private final BuildPropertiesResolved buildPropertiesResolved;

    @Autowired
    public RestrictionService(BuildPropertiesResolved buildPropertiesResolved) {
        this.buildPropertiesResolved = buildPropertiesResolved;
        template = new RestTemplate();
    }

    public ResponseModel consultRestrictionCPF(String cpf) {
        try {
            UriComponents uri = buildPropertiesResolved.buildUri(PATH_API_V1_RESTRICTION + cpf);
            ResponseEntity<Void> entity = template.getForEntity(uri.toUriString(), Void.class);

            return ResponseModel.buildResponse(entity);
        }
        catch (HttpClientErrorException e) {
            return ResponseModel.buildResponse(e.getStatusCode());
        }
    }
}
