package com.sicredi.desafio.service;

import com.sicredi.desafio.core.BuildPropertiesResolved;
import com.sicredi.desafio.model.ResponseModel;
import com.sicredi.desafio.model.SimulationModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;

@Service
public class SimulationService {

    public static final String PATH_API_V1_SIMULATION = "/api/v1/simulacoes/";
    private final BuildPropertiesResolved buildPropertiesResolved;
    private final RestTemplate template;

    @Autowired
    public SimulationService(BuildPropertiesResolved buildPropertiesResolved) {
        this.buildPropertiesResolved = buildPropertiesResolved;
        template = new RestTemplate();
    }

    public ResponseModel create(SimulationModel request) {
        try {
            UriComponents uri = buildPropertiesResolved.buildUri(PATH_API_V1_SIMULATION);
            ResponseEntity<SimulationModel> entity = template.postForEntity(uri.toUriString(), request, SimulationModel.class);

            return ResponseModel.buildResponse(entity);
        }
        catch (HttpClientErrorException e) {
            return ResponseModel.buildResponse(e.getStatusCode());
        }
    }

    public ResponseModel edit(String cpf, SimulationModel request) {
        try {
            UriComponents uri = buildPropertiesResolved.buildUri(PATH_API_V1_SIMULATION + cpf);
            HttpEntity<SimulationModel> httpRequest = new HttpEntity<>(request);
            ResponseEntity<SimulationModel> entity = template.exchange(uri.toUriString(), HttpMethod.PUT, httpRequest, SimulationModel.class);

            return ResponseModel.buildResponse(entity);
        }
        catch (HttpClientErrorException e) {
            return ResponseModel.buildResponse(e.getStatusCode());
        }
    }

    public SimulationModel[] getAll() {
        try {
            UriComponents uri = buildPropertiesResolved.buildUri(PATH_API_V1_SIMULATION);
            ResponseEntity<SimulationModel[]> entity = template.getForEntity(uri.toUriString(), SimulationModel[].class);

            return entity.getBody();
        }
        catch (HttpClientErrorException e) {
            ResponseModel response = ResponseModel.buildResponse(e.getStatusCode());
            if (response.isStatusNoContent()) {
                return new SimulationModel[0];
            }
            return null;
        }
    }

    public ResponseModel getByCpf(String cpf) {
        try {
            UriComponents uri = buildPropertiesResolved.buildUri(PATH_API_V1_SIMULATION + cpf);
            ResponseEntity<SimulationModel> entity = template.getForEntity(uri.toUriString(), SimulationModel.class);

            return ResponseModel.buildResponse(entity);
        }
        catch (HttpClientErrorException e) {
            return ResponseModel.buildResponse(e.getStatusCode());
        }
    }

    public ResponseModel delete(Long id) {
        try {
            UriComponents uri = buildPropertiesResolved.buildUri(PATH_API_V1_SIMULATION + id);
            ResponseEntity<String> entity = template.exchange(uri.toUriString(), HttpMethod.DELETE, new HttpEntity<>(id),  String.class);

            return ResponseModel.buildResponse(entity);
        }
        catch (HttpClientErrorException e) {
            return ResponseModel.buildResponse(e.getStatusCode());
        }
    }

    public void deleteAll() {
        SimulationModel[] all = getAll();
        for (SimulationModel simulationModel : all) {
            UriComponents uri = buildPropertiesResolved.buildUri(PATH_API_V1_SIMULATION + simulationModel.getId());
            template.delete(uri.toUriString());
        }
    }
}
