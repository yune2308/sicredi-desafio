package com.sicredi.desafio.core;

import java.math.BigDecimal;

public class Constants {

    public static final long ID = 11L;
    public static final BigDecimal VALOR_5000 = new BigDecimal(5000);
    public static final String NOME = "Sicredi Alias";
    public static final String CPF = "12345678900";
    public static final String EMAIL = "sicredi@example.com";
    public static final int PARCELAS = 5;
    public static final boolean SEGURO = true;

    public static final BigDecimal VALOR_50000 = new BigDecimal(5000);
    public static final String EMAIL_EXAMPLE = "example@example.com";
    public static final String CPF_WITH_RESTRICTION = "97093236014";
    public static final String CPF_VALID = "43694146089";
    public static final String CPF_NOT_INSERTED = "22233344455";
}
