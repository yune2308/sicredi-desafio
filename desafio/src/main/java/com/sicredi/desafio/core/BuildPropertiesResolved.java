package com.sicredi.desafio.core;

import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class BuildPropertiesResolved {

    @Value("${scheme}")
    @Setter private String scheme;

    @Value("${host}")
    @Setter private String host;

    @Value("${port}")
    @Setter private String port;

    public UriComponents buildUri(String path){
        return UriComponentsBuilder.newInstance()
                .scheme(scheme)
                .host(host)
                .port(port)
                .path(path)
                .build();
    }
}
